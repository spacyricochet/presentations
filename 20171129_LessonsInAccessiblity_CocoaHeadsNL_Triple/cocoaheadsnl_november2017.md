build-lists:true

![inline](noodlewerk_logo.png)

---

## Bruno Scheele

### [noodlewerk.com](http://noodlewerk.com)

### [binaryadventures.com](http://binaryadventures.com)

---

# Lessons in Accessibility

---

# 🙌

^ Who in the audience develops with accessibility in mind?

^ Who draws the line; 100% accessibiliy, or no release?

---

# 💬

* Why accessibility?
* Getting started
* Low-hanging fruit
* Lessons learned
* Where from here?

---

# Why accessibility?

---

### TLDR;

## Make the world<br />a better place

---

> We believe that technology should be accessible to everyone.

[.footer: https://www.apple.com/accessibility/]

---

![](wheelchair.jpg)
![](blind.jpg)
![](quadrileptic.jpg)

^ Treating people equally.

---

## It happens to everyone

^ Everyone needs accessibility some times

^ Broken arm, reading glasses, playing games at night

^ iPhone Plus and X

---

![inline autoplay loop](dynamic_type.mov)

---

## Accessible
## ==
## Better code

^ ABC

^ UI Testing

---

# Getting started

---

## Get acquainted

#### https://www.youtube.com/user/Apple

![inline](apple_accessibility_youtube.png)

---

## Use it yourself

#### Settings > General > Accessibility > Accessibility Shortcut

![inline fit](accessibility_shortcut_menu1.jpg) ![inline fit](accessibility_shortcut_menu2.jpg) ![inline fit](accessibility_shortcut_menu3.jpg) ![inline fit](accessibility_shortcut_menu4.jpg) 

---

![inline autoplay loop](accessibility_shortcut.mov)

---

## Read the documentation

#### https://developer.apple.com/accessibility/

---

## Just start coding

```swift
let addButton = UIButton.buttonWithType(.system)
let title = 
  NSLocalizedString("Add", 
  comment: "Title for adding something.")
button.setTitle(title, forState: .normal)
let accessibilityText = 
  NSLocalizedString("Add a new photo to this meetup.", 
  comment: "Explanation that this adds a meetup photo.")
button.accessibilityLabel = accessibilityText

```

^ Hacktober

---

# Low-hanging fruit

![inline](cocoaheadsnl_logo.png)

---

### UIKit is your friend!

---

![inline](allthethings.png)

* `accessibilityLabel`
* `accessibilityHint`
* `accessibilityTrait`
* `accessibilityIdentifier`

---

### Improve your<br />navigation bar headers

---

![inline](cocoaheadsnl_app_events.jpg)

> CocoaHeadsNL. Header. Image.

---

```swift
let img = UIImageView(image: UIImage(named: "Banner"))
img.accessibilityTraits = 
  img.accessibilityTraits & ~UIAccessibilityTraitImage
img.accessibilityTraits = 
  img.accessibilityTraits | UIAccessibilityTraitHeader

navigationItem.titleView = img
let title = NSLocalizedString("Upcoming and past events",
  comment: "Title for the events list.")
navigationItem.title = title
```

---

![inline](cocoaheadsnl_app_events.jpg)

> Upcoming and past events. Header.


---

### Improve your target areas

---

![inline](cocoaheadsnl_app_jobs.jpg)

> iOS developer.

---

#### Put your subviews in a container view

![inline fit](jobs_cell_layout_before.png) ![inline](jobs_cell_layout_after.png)


#### And add a hint; 'What does this do?'

---

```swift
label.isAccessibilityEnabled = false
image.isAccessibilityEnabled = false
container.isAccessibilityEnabled = true

let text = NSLocalizedString("%1$@ at %2$@",
  comment: "Title with <job> at <company>.")
let accessibilityLabel = 
  String(format: text, job.title, job.company.name)
container.accessibilityLabel = accessibilityLabel

let accessiblityHint = NSLocalizedString("Double-tap for more details.",
  comment: "Explanation that double-tapping shows more details.")
container.accessibilityHint = accessibilityHint
```

---

> Programming Hero at <br />Noodlewerk Apps. Button.
> _<longer pause>_
> Double-tap for more details.

---

### Group accessible text together

![inline autoplay loop](cocoaheadsnl_app_about.mov)

---

```swift
let what =       ""// Text placeholder.
let whatAnswer = ""// Longer text placeholder.
let how =        ""// Text placeholder.
let howAnswer =  ""// Longer text placeholder.

whatLabel.text = what
whatAnswerLabel.text = whatAnswer
howLabel.text = how
howAnswerLabel.text = howAnswer

tableHeaderView.isAccessibilityElement = true
tableHeaderView.accessibilityLabel = 
   [what, whatAnswer, how, howAnswer].
   joined(separator: " ")

```

---

### Special gestures

* `accessibilityPerformEscape`<br />Just like `ESC`.

* `accessibilityPerformMagicTap`<br />Performs the often-used or most-intended action.

---

# Lessons learned

---

# Lessons learned

* Anyone can do it!
* You need more text
* Like explaining your app in code
* Points out flaws in your design…
* … and in your architecture
* Practice makes perfect

---

# Where from here?

* Get your team on board
* UI Testing
* Accessibility Inspector
* _"Advanced"_ accessibility
* Dynamic Type & color schemes
* …

---

## **Test with users!**

![](user_test.jpg)

---

# More information

| _Talk_ | _URL_ |
| --- | --- |
| What's new in accessibility *– WWDC2016_215* | https://developer.apple.com/wwdc17/215 |
| Building apps with Dynamic Type *– WWDC2017_245* | https://developer.apple.com/wwdc17/245 |
| Auditing your apps accessibility *– WWDC2016_407* | https://developer.apple.com/wwdc16/407 |
| Real World Accessibility *- Sommer Panage* | https://vimeo.com/235317172 |
| Architecting a robust color system *– Laura Ragone* | https://news.realm.io/news/architecting-a-robust-color-system-swift-tryswift-2017-ragone/ |

---

# ⁉️

---

# Thank you!

### @brunoscheele

---

# Photo credits

* [Wheelchair basketball – United States Airforce](http://www.afspc.af.mil/News/Article-Display/Article/249811/air-force-wheelchair-basketball-team-preps-for-intense-competition/)
* [Blind drummer – Designed for Carlos V. – Apple](https://youtu.be/EHAO_kj0qcA)
* [Quadrileptic – Cristopher Reeve – Wikipedia](https://en.wikipedia.org/wiki/Christopher_Reeve)
* [All the things - The Internet](http://knowyourmeme.com/memes/all-the-things)

[.autoscale: true]
[.build-lists: false]