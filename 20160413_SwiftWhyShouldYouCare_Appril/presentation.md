# *Swift*

# Why should you care?

---

# Who am I?

## *Bruno Scheele*

* Programming since 1996
* Computer Science @ TUDelft
* iOS Dev @ Noodlewerk Apps

^ Start; Hacking games with Basic and making ugly websites with HTML
^ College; Java, C, C++ and C#
^ Now; Objective-C and Swift, some Ruby

---

* History
* What is Swift?
* Why should you care?
* We want it now!
* Epilogue

---

![](state-of-the-union.jpg)

# A history of Swift

^ Photo credit: Chuck Kennedy, 2011 (public domain)

---

# History – The dark ages 🏰

* Announced at WWDC 2014
* **1.0** released in September 2014
* **1.1** released in October 2014
* **1.2** appeared in April 2015

^ 8 months with lots of crashes and frustation…

---

# History – Modern times 🏙

* **2.0** fixed a lot in September 2015.
* **2.1** followed quickly in October 2015.
* 🎁🎉 Open Source as end-of-year gift! 🍾🎊
* **2.2** release just last month, March 2016

^ The important bit is the open source bit!

---

# Future – Things to come…

Swift 3.0

AKA "We'll stop breaking your code."

# 😎

---

![](swift-logo.png)

# What is Swift?

^ Lofty ambitions. Apple wants Swift to become mainstream.

---

![left](swift-logo.png)

# What is Swift?

Apple;

> Swift is a general-purpose programming language built using a modern approach to safety, performance, and software design patterns.

also…

> Swift is intended as a replacement for C-based languages…

source: [swift.org](http://swift.org)

---

![left](swift-logo.png)

# What is Swift?

Main features;

* Fast
* Safe
* Expressive

---

# Fast

![](swift-performance-language.jpg)

^ Photo credit: [ExtremeTech](http://www.extremetech.com/computing/183563-apples-new-swift-language-explained-a-clever-move-to-boost-ios-while-holding-android-apps-back)

---

# Fast

Two types of *fast*;

1. Meant as a replacement for --> *C* <-- 😱
2. Easier to pick for beginning programmers

---
![](safety-keyboard.jpg)

# Safe

^ Modified photo credit: [Got Credit @ Flickr](https://www.flickr.com/photos/jakerust/)

---

# Type safety

Let's try;

```swift
var x = 42
x += 6
print(x)  // 48
```

---

# Type safety

How about this?

```swift
var x = 42
x += 0.2
print(x)  // So, what does this print?
```

---

# Mutations

Another one;

```swift
let x = 42  // 🤔
x += 6
print(x)    // And now?
```

---

# Nullability

And this one!

```swift
var x = 42
x = nil
print(x)  // what happens here?
```

---

# Optionals

But what if you want `nil`?

```swift
var x: Int? = 42  // the '?' indicates an Optional
x = nil           // this works now!
```

---

# if-statements

```swift
var x: Int? = nil
if let x = x {
    print("x has value \(x)")
} else {
    print("Nothing here.")
}
```

---

# guard-statements

```swift
var x: Int? = nil
guard let x = x where x > 0 else {
        print("\(x) is nil or negative")
        return
}
print("x has value \(x)")
```

---

![](starry-night.jpg)

# Expressive

^ Image credit: Starry Night by Van Gogh

---

# Functions are first-class citizens

```swift
func doMath
    (lhs: Int, _ rhs: Int, _ operation: (Int, Int) -> Int) -> Int 
{
    return operation(lhs, rhs)
}

doMath(3, 2, *)    // 6
doMath(3, 2, -)    // 1
```

^ Lots of languages have this, but it's a nice to-have that feels missing if it's not there.

---

# Generics

```swift
func doMath<T where T: SignedNumberType>
    (lhs: T, _ rhs: T, _ operation: (T, T) -> T) -> T 
{
    return operation(lhs, rhs)
}

doMath(3, 2, *)        // 6
doMath(3.0, 2.5, -)    // 0.5
```

^ This is a lot of fun, though it can make your brain hurt.

---

# Playing with types

```swift
Pouch() 
    <<< 5.gold 
    <<< 8.platinum 
    <<< 900.silver

// Loot! 💰
```

---

![](swift-logo.png)

# Why should you care?

---

* Future for Apple
* New devs
* Cross-platform

---

# Future for Apple

* All-in on Swift
* Open-source
* Cloud services
* Larger pool of developers

---

Are you developing native 
for Apple platforms?

**WHY NO SWIFT?**

# 😫

---

![](baby-chick.jpg)

# New Devs

---

# Very newbie-friendly 🐤

* Easy to pick up
* Clarity before cleverness
* Compiler errors vs. runtime exceptions
* Thriving community

---

# Playful learning

* Xcode and playgrounds
* Ideal for courses

![left](swift-playground.jpg)

^ Image credit: Apple

---

# Any programming style

If you learned it, you can probably use it in Swift

* Imperative
* Procedural
* Functional
* Object-oriented
* Protocol-oriented

^ Useful for advanced programmers too

---

![](mario-platforms.jpg)

# Cross-platform

^ Image credit: Super Mario Bros. altered screenshot

---

> Write once, run everywhere

![inline](holy-grail.jpg)

^ Java, Javascript, C
^ Lock-in with Apple?

^ Image credit: Monty Python and the Holy Grail screencap

---

![fill](tux.png)
![fill](ibm-swift.png)
![fill](raspberry-pi.jpg)
![fill](google-nexus-6p.jpg)

---

# Swift on Linux

---

# Swift on Linux

"Easy" part
Out of the box support

[https://swift.org/getting-started](https://swift.org/getting-started)

---

# Xcode Swift != Vanilla Swift

```swift
func doSomething() {
    do { try somethingThatThrows() }
    catch { // StackOverflow says this works! 🤕
        print("Something happened!")
    }
}
```

^ The `catch` does not compile :(

---

# Xcode Swift != Vanilla Swift

```swift
func doSomething() {
    do { try somethingThatThrows() }
    catch let error as NSError {
        // handle error
    }
}
```

[Practical cross-platform Swift](https://realm.io/news/tryswift-jp-simard-practical-cross-platform-swift/)  by JP Simard[^1]

[^1]: https://realm.io/news/tryswift-jp-simard-practical-cross-platform-swift/

^ Instead, be explicit.

---

# IBM ❤️ Swift

---

# IBM ❤️ Swift

## end-to-end applications in one language

![inline](ibm-swift.png)

Example: [NSDateformatter.com](http://nsdateformatter.com)

---

# IBM Swift Sandbox[^2]

![inline](ibm-swift-sandbox.png)

[^2]: https://developer.ibm.com/swift

^ Image credit: IBM – Hour of Code

---

# Swift with Raspberry Pi

---

# Swift on Raspberry Pi

![inline](raspberry-pi.jpg)

^ Should be the same as on Linux

---

* Compiling properly on ARM
* Getting everything to work
* Great efforts

More info: <br>Swift on Raspberry Pi by Andrew Madsen[^3]

[^3]: http://blog.andrewmadsen.com/post/136137396480/swift-on-raspberry-pi

^ Sadly, no Arduino.

---

# 🤖 + 🍎 = 😍

## Swift on Android

---

# Swift on Android

Recent tabloid headlines

> The Next Web:
> "Google is said to be considering Swift as<br> a ‘first class’ language for Android"[^4]

[^4]: http://thenextweb.com/dd/2016/04/07/google-facebook-uber-swift/

^ Scientists believe there may be intergalactic life out there.

---

![](shut-up-and-take-my-money.jpg)

# We want it now!

---

# How to start…

* Read up on Swift at [swift.org](http://swift.org)
* On a Mac? Download Xcode and create a Playground 🎈
* Create your own simple scripts[^5]

[^5]: http://krakendev.io/blog/scripting-in-swift

---

# Stay up to date

* Swift newsletter by Natasha the Robot[^6]
* Erica Sadun's blog with lots of Swift proposals[^7]
* swift-evolution on Github[^8]

[^6]: https://natashatherobot.com

[^7]: http://ericasadun.com

[^8]: https://github.com/apple/swift-evolution

---

# Still not convinced?

![inline](swift-is-hiring.png)

^ Image credit: [Screencap of @jckarter on Twitter](https://twitter.com/jckarter/status/719585924338753537)

---

# *Thank you!*

<br>
## Any questions?

<br><br><br>
@brunoscheele

bruno@noodlewerk.com

---
