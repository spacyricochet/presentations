//: Playground - noun: a place where people can play

import UIKit

func doMath<T where T: SignedNumberType>(lhs: T, _ rhs: T, _ operation: (T, T) -> T) -> T {
    return operation(lhs, rhs)
}

doMath(2, 3, *)
doMath(2.1, 3.4, +)

var x = 42
x += 6
print(x)

//x = x + 0.2

func mul(x: Int, _ y: Int) -> Int {
    return x * y
}

doMath(2, 3, mul )

