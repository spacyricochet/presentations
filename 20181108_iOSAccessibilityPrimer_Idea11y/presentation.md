build-lists: true
<!-- slidenumbers: true -->

# [fit] iOS Accessibility<br />Primer

#### <br />*Bruno Scheele, Noodlewerk Apps*

---

# [fit] 🍏

[.footer: Image: Green apple emoji]

---

# Table of Contents

1. Why accessibility?
2. iPhone Accessibility
3. Using it yourself
4. Developing for Accessibility
5. Want to know more?

---

# Why Accessibility?

---

### TLDR;

## Make the world<br />a better place

---

> We believe that technology should be accessible to everyone.

[.footer: Apple – https://www.apple.com/accessibility/]

---

![](wheelchair.jpg)
![](blind.jpg)
![](quadrileptic.jpg)

^ Captions on next slide.

---

![](wheelchair.jpg)
![](blind.jpg)
![](quadrileptic.jpg)

Photo 1 | Photo 2 | Photo 3
:---|:---:|---:
Playing basketball in a wheelchair | Creating music as a blind person | Being a hero and<br />quadrileptic

^ Captioned version of photo slide.

^ Treating people equally.

---

## It happens to everyone

---

# Everyone needs accessibility at some point

* Broken arm
* Reading glasses
* Playing games at night
* Huge phones

---

# [fit] iPhone<br />Accessibility

---

![inline](applewebsite.png)

[.footer: Image: Apple Accessibility website screenshot – Apple – https://www.apple.com/accessibility/iphone]

---

# Four categories

1. Vision
2. Hearing
3. Physical and Motor Skills
4. Learning and Literacy

---

## Physical and Motor Skills<br />

# Assistive Touch

![right fit](assistivetouch.png)

[.footer: Image: iPhone screen with Assistive Touch enabled, shown as a darkened rectangle with six shortcut buttons. – Apple – https://www.apple.com/accessibility/iphone/physical-and-motor-skills/]

---

## Physical and Motor Skills<br />

# Switch Control

![right fit autoplay loop](switchcontrol.mov)

[.footer: Video: Switch control demo. Captions on next slide.]

---

```
'Switch control demo' video captions:

1. iPhone screen showing Switch Control.
2. Switch Control is shown by a focus rectangle
   switching UI elements.
3. The focus rectangle initially focuses on
   entire element groups or sections.
4. After selecting, are elements focused on 
   individually.
5. Selecting an element activates it.
6. Some screens are navigated by using Switch
   control.
```

---

## Hearing<br />

# Hearing Aids support

[.footer :Image: iPhone showing hearing aid settings, with hearing aids in front – Apple – https://www.apple.com/accessibility/iphone/hearing]

![right fit](hearingaids.png)

---

## Hearing<br />

# Captions

[.footer: Image: iPhone showing movie with captioned subtitles – Apple – https://www.apple.com/accessibility/iphone/hearing]

![right fit](captions.png)

---

## Vision<br />

# Dynamic font sizes

![right fit autoplay loop](dynamic_type.mov)

[.footer: Video: Dynamic font sizes demo. Captions are on the next slide.]

---

```
'Dynamic font sizes' video captions:

1. An iPhone screen is shown with the Meetup app 
   displayed.
2. The user slides the iOS Control Center down, 
   which contains dynamic type button.
3. The user taps the dynamic type control to show 
   the adjustment slider.
4. The user slides the text size up to maximum.
5. The user goes back to the Meetup app.
6. The text is shown in a larger font size.
```

^ Captions for the video.

---

## Vision<br />

# Colour filters

![right fit autoplay loop](colourfilters.mov)

[.footer: Video: Colour filters demo. Captions are on the next slide.]

---

```
'Colour filters' video captions:

1. Showing the colour filter options.
2. Sliding between different color representations;
   pencils, primary and secondary colors, and
   hexagonal color map.
3. Turning colour filters on. Options appear. 
   Greyscale is selected. Screen turns greyscale.
4. Some more color filters are selected; 
   protanopia, deuteranopia, tritanopia.
   Screen changes accordingly after each selection.
5. Custom color filters are also available.
```

^ Captions for the video.

---

## Using it yourself

---

## Using it yourself

1. Go to Settings
2. Go to General
3. Go to Accessibility
4. Scroll all the way down
5. Go to Accessibility Shortcut
6. Enable one or more options
7. Triple tap Lock or Home

![right fit autoplay loop](accessibilityshortcut.mov)

[.footer: Video: How to turn on Accessibility Shortcut on the iPhone. Explained in same slide.]

---

## Developing for Accessibility

---

#### Before anything

## Design for<br />Accessibility first!

---

# Four 'easy' wins

1. Use standard components
2. Dynamic font sizes
3. Colour themes
4. Voice Over

---

# Standard components

![inline fit](standardcomponents.png)

[.footer: Image: Two iPhones showing table views with standard icons, controls and image styles – Apple – https://developer.apple.com/design/human-interface-guidelines/ios/views/tables/]

---

# Dynamic font sizes

* Use the system 'font styles' as much as possible
* Or use calculated fonts instead
* If you can't better make them big
* Don't forget to account for resizing and scrolling

![right fit](fontstyles.png)

[.footer: Image: All iOS's font styles, displayed in extra small, large and extra extra large – Use your Loaf – https://useyourloaf.com/blog/using-a-custom-font-with-dynamic-type/]

---

# Colour themes

* Start with color themes
* Provide at least a dark alternative
* Account for 'Invert colours'
* Use font themes too

![right fit autoplay loop](colourthemes.mov)

[.footer: Video: Overcast app settings to switch between color themes. Captions on next slide. – https://overcast.fm]

---

```
'Colour themes' video captions:

1. Overcast app settings screen for changing color themes.
2. Black is selected. 
   The app uses mostly white on black.
3. Light is selected. 
   The app now uses mostly black on white.
4. Dark is selected.
   The app now uses mostly white on dark gray.
5. Dark is selected again.
6. System font is turned on. 
   The font style changes to the default font.
7. System font is turned off again.
   The font style changes to the app's custom font.
```

---

# Voice Over

![right fit](voiceover.mov)

[.footer: Video: Voice Over demo. Captions on next slide.]

^ Check sound. No autoplay.

---

```
'Voice Over demo' video captions (1 of 2):

1. Game Counter app is shown in Voice Over.
   The currently focused element is the title, 
   surrounded by a focus rectangle.
2. The following is read aloud:
   * Game Counter. Button. Double-tap to read 
   more about this app.
3. The focus moves over some selected cells.
4. The following is read aloud:
   Bruno. Selected.
5. The focus moves over an unselected cell.
   The following is read aloud:
   * Vaevictis.
6. Vaevictis becomes selected.
   The following is read aloud:
   * Vaevictis selected.

Captions continue on next slide.   
```

---

```
'Voice Over demo' video captions (2 of 2):

7.  A new game is started.
8.  The focus is set to the current player.
    The player's total score and subscores are
    read aloud.
9.  The focus is set to a subscore of one.
    The following is read aloud:
    * Level. 1. Adjustable. Swipe up or down to adjust.
10. After adjusting, entire score is read aloud.
11. Focus moves to current player in a player list.
    After the player announcement, the following 
    is read aloud:
    * It is Bruno's turn now.
12. Focus moves to the another player.
    After the player announcement, the following 
    is read aloud:
    * Double-tap to give Scavo the turn.
```

---

# Voice Over

* Visible label or image (fallback)
* Accessible label
* Component traits<br />(e.g. 'button' or 'disabled')
* Usage hint<br />(e.g. 'Double-tap to give Bruno the turn')

^ Start early! Lots of work.

^ Don't forget to localize!

---

# Want to know more?

---

# Want to know more?

* **iOS Accessibility Tips and Tricks** – Sommer Panage (https://youtu.be/dmMASdKhl_w)
* **Accessibility: A line of code can be many things** – Sally Shepard (https://www.dotconferences.com/2018/01/sally-shepard-accessibility)
* **Accessible podcast** – Steven Aquino and Timothy Buck (https://accessible.fm)

---

# Any questions?

#### bruno@noodlewerk.com<br />or<br />@brunoscheele
